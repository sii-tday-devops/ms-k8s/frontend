FROM nginx:alpine

COPY dist/frontend /app/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
