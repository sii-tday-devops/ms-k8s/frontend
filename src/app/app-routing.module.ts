import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'establishments',
    loadChildren: () => import('./app/establishments/establishments.module').then(m => m.EstablishmentsModule),
  },
  {
    path: 'establishments/new',
    loadChildren: () => import('./app/establishments/new/new.module').then(m => m.NewModule),
  },
  {
    path: 'establishments/details/:id',
    loadChildren: () => import('./app/establishments/details/details.module').then(m => m.DetailsModule)
  },
  {
    path: '',
    redirectTo: 'establishments',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
