import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { EstablishmentsService } from '../../shared/establishments.service';
import { CommentsService } from '../../shared/comments.service';
import { PicturesService } from '../../shared/pictures.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  id;
  establishment;
  comments;
  pictures;

  comment = new FormGroup({
    establishment: new FormControl(),
    author: new FormControl(),
    content: new FormControl(),
  });

  constructor(
    private route: ActivatedRoute,
    private establishmentsService: EstablishmentsService,
    public commentsService: CommentsService,
    public picturesService: PicturesService,
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.establishment = this.establishmentsService.get(this.id);
    this.comments = this.commentsService.list();
    this.pictures = this.picturesService.list();
  }

  onSubmitComment() {
    let data = this.comment.value;
    data.establishment = this.id;
    this.commentsService.create(data).subscribe();
  }
}
