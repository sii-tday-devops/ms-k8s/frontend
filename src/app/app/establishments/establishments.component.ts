import { Component, OnInit } from '@angular/core';
import { EstablishmentsService } from '../shared/establishments.service';

@Component({
  selector: 'app-establishments',
  templateUrl: './establishments.component.html',
  styleUrls: ['./establishments.component.scss']
})
export class EstablishmentsComponent implements OnInit {
  establishments;

  constructor(private establishmentsService: EstablishmentsService) { }

  ngOnInit() {
    console.log('init');
    this.establishmentsService.list().subscribe((data) => {
      this.establishments = data;
    });
  }

}
