import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EstablishmentsRoutingModule } from './establishments-routing.module';
import { EstablishmentsComponent } from './establishments.component';
import { MatIconModule, MatButtonModule, MatCardModule, MatGridListModule } from '@angular/material';


@NgModule({
  declarations: [EstablishmentsComponent],
  imports: [
    CommonModule,
    EstablishmentsRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
  ]
})
export class EstablishmentsModule { }
