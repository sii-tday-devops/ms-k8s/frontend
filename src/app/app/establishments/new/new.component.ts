import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { EstablishmentsService } from '../../shared/establishments.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {
  establishment = new FormGroup({
    name: new FormControl(),
    phone: new FormControl(),
    address: new FormControl(),
    position: new FormControl(),
  });

  constructor(private establishmentsService: EstablishmentsService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.establishmentsService.create(this.establishment.value).subscribe();
  }
}
