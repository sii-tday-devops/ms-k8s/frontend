import { Injectable } from '@angular/core';
import { CrudService } from './crud.service';

@Injectable({
  providedIn: 'root'
})
export class CommentsService extends CrudService {

  protected baseUrl = '/api/comments/v1';
}
