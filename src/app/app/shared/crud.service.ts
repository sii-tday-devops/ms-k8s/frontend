import { HttpClient } from '@angular/common/http';

export class CrudService {

  protected baseUrl = '/api/';

  constructor(protected httpClient: HttpClient) { }

  public list() {
    return this.httpClient.get(this.baseUrl);
  }

  public create(body) {
    return this.httpClient.post(this.baseUrl, body);
  }

  public get(id) {
    return this.httpClient.get(this.baseUrl + '/' + id);
  }

  public update(id, body) {
    return this.httpClient.put(this.baseUrl + '/' + id, body);
  }

  public delete(id) {
    return this.httpClient.delete(this.baseUrl + '/' + id);
  }
}
