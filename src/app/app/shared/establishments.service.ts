import { Injectable } from '@angular/core';
import { CrudService } from './crud.service';

@Injectable({
  providedIn: 'root'
})
export class EstablishmentsService extends CrudService {

  protected baseUrl = '/api/establishments/v1';
}
