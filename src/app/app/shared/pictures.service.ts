import { Injectable } from '@angular/core';
import { CrudService } from './crud.service';

@Injectable({
  providedIn: 'root'
})
export class PicturesService extends CrudService {

  protected baseUrl = '/api/pictures/v1';
}
